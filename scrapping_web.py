from urllib.request import urlopen
from bs4 import BeautifulSoup
import ssl
import sqlite3

def info_player(pos,tag):

    if pos == "BAT":
        info_std = ["equipo","juegos","turnos","Carreras", "H","Dobles","Triples","HR","CI","BB",\
        "P","B.Robadas","B.NoRobadas","AVG","OBP","Slugging","OPS"]
        col_info = 17
    else:
        info_std = ["equipo","J.Ganados","J.Perdidos","C.Limpias%","juegos","Aperturas",\
            "J.Completados","J.Blanqueados","J.Salvados","Oportunidades.Sal","Entradas.Lan","H",\
            "Carreras","C.Limpias","HR","Bat.Golpeados","BB","P","WHIP","AVG"]
        col_info = 20
    
    jugador=dict()
    #se hace esta excepcion porque la pasada 0 es la de los encabezados en cada pagina
    
    jugador["nombre"]=tag.a.get('aria-label',None)
    jugador["posicion"]=tag.find_all('div','position-28TbwVOg')[0].text
    
    #recorre y toma el dato de cada columna en la pagina lo asigna de acuerdo a la informacion
    for col in range(1,col_info+1):
        jugador[info_std[col-1]]=tag.find_all(attrs={"data-col":str(col)})[0].text
        #conversion de dato segun criterio STR, INT, FLOAT
        if col == 1:
            continue
        elif (col>13 and col_info == 17) or (col == 4 and col_info == 20)\
             or (col == 11 and col_info == 20) or col == 19 or col == 20 :
            jugador[info_std[col-1]]=float(jugador[info_std[col-1]])
        else:
            jugador[info_std[col-1]]=int(jugador[info_std[col-1]])
    
    return jugador

conn = sqlite3.connect('playerdb.sqlite')
cur = conn.cursor()

cur.executescript(''' 

CREATE TABLE IF NOT EXISTS Position (
    id  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    position_name    TEXT UNIQUE
);

CREATE TABLE IF NOT EXISTS Team (
    id  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    team_name   TEXT UNIQUE
);

CREATE TABLE IF NOT EXISTS Player_BAT (
    id  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    player_name TEXT  UNIQUE,
    team_id  INTEGER, 
    position_id INTEGER,
    juegos INTEGER, turnos INTEGER, carreras INTEGER, hit INTEGER, Dobles INTEGER, Triples INTEGER,
    HR INTEGER, CI INTEGER, BB INTEGER, Ponchado INTEGER, B_Robadas INTEGER, B_NoRobadas INTEGER,
    AVG_Bat FLOAT, OBP FLOAT, Slugging FLOAT, OPS FLOAT
);

CREATE TABLE IF NOT EXISTS Player_PIT (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    player_name TEXT UNIQUE,
    team_id INTEGER,
    position_id INTEGER,
    J_Ganados INTEGER, J_Perdidos INTEGER, C_LimpiasPerc FLOAT, juegos INTEGER,
    Aperturas INTEGER, J_Completados INTEGER, J_Blanqueados INTEGER, J_Salvados INTEGER,
    Oportunidades_Sal INTEGER, Entradas_Lan FLOAT, H INTEGER, Carreras INTEGER, C_Limpias INTEGER,
    HR INTEGER, Bat_Golpeados INTEGER, BB INTEGER, P INTEGER, WHIP FLOAT, AVG_EnContra FLOAT
);''')

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

#URL base y derivaciones para obtener informacion de los playeres BAT and PIT
url = "https://www.mlb.com/es/stats/"
url_bat = "?playerPool=ALL"
url_pit = "pitching"

webpage=1 #Contador para cambiar las paginas

for i in range(2):
    while True:
        #Se toma las variaciones de la URL para navegar por las diferentes paginas
        if webpage == 1 and i == 0:
            html = urlopen(url+url_bat, context=ctx).read()
            pos_fun="BAT"
        elif webpage != 1 and i == 0:
            html = urlopen(url+"?page="+str(webpage)+"&"+url_bat[1:], context=ctx).read()
        elif webpage == 1 and i == 1:
            html = urlopen(url+url_pit, context=ctx).read()
            pos_fun="PIT"
        else:
            html = urlopen(url+url_pit+"?page="+str(webpage), context=ctx).read()
        
        #utilizamos la libreria BS4 para hacer el web scrapping y se toma la etiqueta que se necesita
        soup = BeautifulSoup(html, "html.parser")
        tags = soup('tr')
        
        count_pass=0

        for info_tag in tags:
            #se hace esta excepcion porque la pasada 0 es la de los encabezados en cada pagina
            if count_pass == 0:
                count_pass += 1
                continue
            
            player=info_player(pos_fun,info_tag)

            #intenta actualizar comparando el numero de juegos
            try:
                cur.execute('SELECT juegos FROM Player_'+pos_fun+' WHERE player_name = ?' ,(player["nombre"], ))
                juegos_old=cur.fetchone()[0]
                if player["juegos"] == juegos_old:
                    print('El jugador',player["nombre"],'tiene los datos actualizados')
                    continue
            except TypeError:
                print(player)

            #escritura de la DB
            cur.execute('''INSERT OR IGNORE INTO Position (position_name) 
                VALUES ( ? )''', ( player["posicion"], ) )
            cur.execute('SELECT id FROM Position WHERE position_name = ? ', (player["posicion"], ))
            position_id = cur.fetchone()[0]

            cur.execute('''INSERT OR IGNORE INTO Team (team_name) 
                VALUES ( ? )''', ( player["equipo"], ) )
            cur.execute('SELECT id FROM Team WHERE team_name = ? ', (player["equipo"], ))
            team_id = cur.fetchone()[0]

            if pos_fun == "BAT":
                cur.execute('''INSERT OR REPLACE INTO Player_BAT
                    (player_name, position_id, team_id, juegos, turnos, carreras, hit, Dobles, Triples, HR, 
                    CI, BB, Ponchado, B_Robadas, B_NoRobadas, AVG_Bat, OBP, Slugging, OPS) 
                    VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )''', 
                    ( player["nombre"], position_id, team_id, player["juegos"],player["turnos"],\
                    player["Carreras"], player["H"], player["Dobles"], player["Triples"],\
                    player["HR"], player["CI"], player["BB"], player["P"], player["B.Robadas"],\
                    player["B.NoRobadas"], player["AVG"], player["OBP"], player["Slugging"],\
                    player["OPS"] ) )
            else:
                cur.execute('''INSERT OR REPLACE INTO PLayer_PIT
                    (player_name, position_id, team_id, J_Ganados, J_Perdidos, C_LimpiasPerc, juegos,
                    Aperturas, J_Completados, J_Blanqueados, J_Salvados, Oportunidades_Sal, Entradas_Lan,
                    H, Carreras, C_Limpias, HR, Bat_Golpeados, BB, P, WHIP, AVG_EnContra) 
                    VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ''',
                    ( player["nombre"], position_id, team_id, player["J.Ganados"], player["J.Perdidos"],\
                    player["C.Limpias%"], player["juegos"], player["Aperturas"], player["J.Completados"],\
                    player["J.Blanqueados"], player["J.Salvados"], player["Oportunidades.Sal"],\
                    player["Entradas.Lan"], player["H"], player["Carreras"], player["C.Limpias"],\
                    player["HR"], player["Bat.Golpeados"], player["BB"], player["P"], player["WHIP"],\
                    player["AVG"] ) )
            conn.commit() 

        #si encuentra una pagina siguiente continua de lo contrario sale del bucle
        if len(soup.find_all(attrs={"aria-label":"next page button"}))>0:
            webpage += 1
            print(url+"?page="+str(webpage))
        else:
            print("Cambio de posicion o finalizacion")
            webpage = 1
            break


